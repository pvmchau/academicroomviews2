<?php

/**
 * Implementation of hook_views_default_views().
 */
function follower_views2_views_default_views() {
  $views = array();

  // Exported view: Followers
  $view = new view;
  $view->name = 'Followers';
  $view->description = 'list of users who subscribe discussion';
  $view->tag = '';
  $view->base_table = 'users';
  $view->core = 0;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'flag_user_content_rel' => array(
      'label' => 'user flagged content',
      'required' => 1,
      'flag' => 'follow',
      'id' => 'flag_user_content_rel',
      'table' => 'users',
      'field' => 'flag_user_content_rel',
      'relationship' => 'none',
    ),
    'content_profile_rel' => array(
      'id' => 'content_profile_rel',
      'table' => 'users',
      'field' => 'content_profile_rel',
    ),
  ));
  $handler->override_option('fields', array(
    'uid' => array(
      'label' => 'Uid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_user' => 1,
      'exclude' => 1,
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'field_user_picture_fid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 1,
        'path' => 'user/[uid]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'user_follower_default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_user_picture_fid',
      'table' => 'node_data_field_user_picture',
      'field' => 'field_user_picture_fid',
      'relationship' => 'content_profile_rel',
    ),
    'nothing' => array(
      'label' => 'Followers',
      'alter' => array(
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 1,
      'id' => 'nothing',
      'table' => 'views',
      'field' => 'nothing',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'content_id' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'node',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'content_id',
      'table' => 'flag_content',
      'field' => 'content_id',
      'relationship' => 'flag_user_content_rel',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        2 => 0,
        3 => 0,
      ),
      'override' => array(
        'button' => 'Override',
      ),
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_image_size' => '_original',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'image' => 0,
        'rotor_item' => 0,
        'aca_book' => 0,
        'achievements' => 0,
        'article' => 0,
        'audio' => 0,
        'basic' => 0,
        'bibliographies' => 0,
        'blog' => 0,
        'blog_post' => 0,
        'book_chapter' => 0,
        'book_review' => 0,
        'conference_paper' => 0,
        'contact' => 0,
        'course_lecture' => 0,
        'date' => 0,
        'discussions' => 0,
        'dissertation' => 0,
        'education' => 0,
        'education_fields_group' => 0,
        'groups' => 0,
        'honors_awards' => 0,
        'images' => 0,
        'journal' => 0,
        'manuscripts' => 0,
        'news' => 0,
        'page' => 0,
        'profession_mem' => 0,
        'profile_achievement' => 0,
        'publications_summary' => 0,
        'slideshow' => 0,
        'story' => 0,
        'syllabus' => 0,
        'topic' => 0,
        'video' => 0,
        'webform' => 0,
        'webpage' => 0,
        'workexperience' => 0,
        'workingpapers' => 0,
        'work_experience_field_group' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        2 => 0,
        24 => 0,
        12 => 0,
        21 => 0,
        19 => 0,
        10 => 0,
        9 => 0,
        7 => 0,
        8 => 0,
        22 => 0,
        16 => 0,
        6 => 0,
        18 => 0,
        11 => 0,
        20 => 0,
        15 => 0,
        23 => 0,
        13 => 0,
        14 => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_node_flag_name' => '*relationship*',
      'validate_argument_node_flag_test' => 'flaggable',
      'validate_argument_node_flag_id_type' => 'id',
      'validate_argument_user_flag_name' => '*relationship*',
      'validate_argument_user_flag_test' => 'flaggable',
      'validate_argument_user_flag_id_type' => 'id',
      'image_size' => array(
        '_original' => '_original',
        'thumbnail' => 'thumbnail',
        'preview' => 'preview',
      ),
      'validate_argument_is_member' => '0',
      'validate_argument_group_node_type' => array(
        'blog' => 0,
        'groups' => 0,
      ),
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Followers');
  $handler->override_option('empty_format', '8');
  $handler->override_option('use_more', 1);
  $handler->override_option('use_more_always', 0);
  $handler->override_option('use_more_text', 'See All');
  $handler->override_option('style_plugin', 'grid');
  $handler->override_option('style_options', array(
    'grouping' => 'nothing',
    'columns' => '5',
    'alignment' => 'horizontal',
    'fill_single_line' => 1,
  ));
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('relationships', array(
    'flag_user_content_rel' => array(
      'label' => 'user flagged content',
      'required' => 1,
      'flag' => 'follow',
      'id' => 'flag_user_content_rel',
      'table' => 'users',
      'field' => 'flag_user_content_rel',
      'relationship' => 'none',
    ),
    'content_profile_rel' => array(
      'label' => 'Content Profile',
      'required' => 0,
      'type' => 'basic',
      'id' => 'content_profile_rel',
      'table' => 'users',
      'field' => 'content_profile_rel',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);

  $views[$view->name] = $view;

  return $views;
}
